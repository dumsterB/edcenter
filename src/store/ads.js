
// class Ad {
//   constructor (title, description, ownerId, imageSrc = '', promo = false, id = null) {
//     this.title = title
//     this.description = description
//     this.ownerId = ownerId
//     this.imageSrc = imageSrc
//     this.promo = promo
//     this.id = id
//   }
// }
export default {
   state: {
      ads: [
         {
            title: 'First ad',
            description: 'description',
            promo: false,
            imageSrc: "https://www.bmw.com/content/dam/bmw/marketBMWCOM/bmw_com/categories/all-models/4-series/grancoupe/2019/bmw-4series-grancoupe-03-quote-hd-d.jpg?imwidth=1280",
            id: '123'
         },

         {
            title: 'second ad',
            description: 'description',
            promo: true,
            imageSrc: "https://www.bmw.com/content/dam/bmw/marketBMWCOM/bmw_com/categories/all-models/2-series/coupe/2019/bmw-2series-coupe-07-nsc-big-d.jpg?imwidth=1280",
            id: '1234'
         },

         {
            title: 'third ad',
            description: 'description',
            promo: true,
            imageSrc: "https://www.bmw.com/content/dam/bmw/marketBMWCOM/bmw_com/categories/all-models/m-series/m235i-xdrive-gran-coupe/bmw-f44-04-nsc-cinema-d.jpg?imwidth=1280",
            id: '12345'
         }
      ],
   },
   mutations: {
      createAd (state, payload) {
        state.ads.push(payload)
      },
      updateAd(state,{title,description,id}){
       const ad = state.ads.find(a =>{
          return a.id===id
       })
       ad.title=title,
       ad.description=description
      }
    },
    actions: {
      async createAd ({commit}, payload) {
        commit('clearError')
        commit('setLoading', true)
        payload.id='fewf'
        commit('createAd',payload)
        await 
        commit('setLoading',false)
      },
      async updateAd ({commit},{title,description,id}){
           commit('clearError')
           commit('setLoading',true)
          try{
             commit('setLoading',false)
             commit('updateAd',{
                title,description,id 
          })
           }
           catch(error){
              commit('setError',error.message)
              commit('setLoading',false)
              throw error
           }
      }
    },
   getters: {
      ads(state) {
         return state.ads
      },
      promoAds(state) {
         return state.ads.filter(ad => {
            return ad.promo == true
         })
      },
      myAds(state) {
         return state.ads
      },
      adById(state) {
         return adId => {
            return state.ads.find(ad => ad.id === adId)
         }
      }
   }

}