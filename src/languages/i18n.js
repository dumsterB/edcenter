import Vue from 'vue'
import Vuei18n from 'vue-i18n'
// import ru from '../languageTitle/ru.json'
// import uz from '../languageTitle/uz.json'
Vue.use(Vuei18n)

export const i18n = new Vuei18n({
    locale:'ru',
    fallbackLocale:'uz',
    messages: {
         ru:{
            "registerLabel":"Регситрация",
            "createAccount":"Создать аккаунт",
            "register":{"title":"регистрация"},
            "login":{"title":"логин"},
            "orders":{"title":"заказы"},
            "new":{"title":"новая реклама"},
            "list":{"title":"рекламы"},
            "logout":"Выйти"
           
        },
        uz:{
            "registerLabel":"Ro'yxatdan o'tish",
            "createAccount":"Hisob yaratish",
            "register" :{"title":"ro'yxatdan o'tkazish"},
            "login":{"title":"kirish"},
            "orders":{"title":"buyurtmalar"},
            "new":{"title":"yangi reklama"},
            "list":{"title":"reklamar"},
            "logout":"Chiqish"
            
        }
}


   
}) 