import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import {i18n} from './languages/i18n'
import vueFlagIcon from 'vue-flag-icon'
import router from  './router/router'
import store from './store'
import * as fb from 'firebase'
import dateFilter from './date/datetime'
// import message from './untils/message'

Vue.config.productionTip = false
Vue.use(vueFlagIcon)
Vue.filter('date',dateFilter)
// Vue.use(message)

new Vue({
 vuetify,i18n,vueFlagIcon,router,store,
  render: h => h(App),
  created(){
    fb.initializeApp({
      apiKey: "AIzaSyADLSnJbuxdNoRQWktXPWCSUzPqe8hSjV8",
      authDomain: "project-id.firebaseapp.com",
      databaseURL: "https://project-id.firebaseio.com",
      projectId: "project-id",
      storageBucket: "project-id.appspot.com",
      messagingSenderId: "sender-id",
      appId: "app-id",
      measurementId: "G-measurement-id",
    }); 
    fb.auth().onAuthStateChanged(user =>{
      if(user){
        this.$store.dispatch('autoLogin',user)
      }
    })
  }
}).$mount('#app')
