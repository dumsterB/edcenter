import Vue from 'vue'
import Router from 'vue-router'
import auth from './authGuard'
Vue.use(Router)
export default new Router({
    routes: [
        {
            path: '/',
            component: () => import('../components/regist/home.vue'),
            name: 'home',
        },
        {
            path: '/login',
            component: () => import('../components/regist/login.vue'),
            name: 'login'
        },

        {
            path: '/orders',
            component: () => import('../components/regist/orders.vue'),
            name: 'orders',
            beforeEnter:auth
        },
        {
            path: '/ad/:id',
            props: true,
            component: () => import('../components/regist/Ad.vue'),
            name: 'Ad',
        },
        {
            path: '/buy/:id',
            props: true,
            component: () => import('../components/regist/buy.vue'),
            name: 'buy',
        },
        {
            path: '/new',
            component: () => import('../components/regist/newAd.vue'),
            name: 'newad',
            beforeEnter:auth
        },
        {
            path: '/registar',
            component: () => import('../components/regist/registration.vue'),
            name: 'register'
        },
        {
            path: '/list',
            component: () => import('../components/regist/AdList.vue'),
            name: 'adlist',
            beforeEnter:auth
        },

    ],
    mode:'history'
})